---
theme: "metropolis"
colortheme: "beaver"
header-includes: |
    \usepackage{amsmath}
    \usepackage{multicol}
    \definecolor{fsired}{RGB}{223, 0, 0}
    \setbeamercolor{frametitle}{bg=fsired, fg=white}
---

\begin{center}
\includegraphics[width=50mm]{logo.png}
\includegraphics[width=50mm]{BioInf.jpg}
\end{center}

Gruppe von motivierten Leuten, vor allem aus dem Fachbereich Informatik

# Was wir machen

- Hochschulpolitik
- Ersti- und Sommerfahrten
- Feiern veranstalten
- Vernetzung und Anlaufstelle für Studierende sein
- Karaoke
- Lebensqualität am Fachbereich verbessern

# Vertretung der Studierenden

- Wir vertreten euch bei:
    - Problemen mit Lehrveranstaltungen
    - Verbesserungswünsche am Fachbereich
    - Allgemeiner Studierenden Ausschuss (AStA)
    - etc.

# Hochschulpolitik

- Gremienarbeit
- Akademische Selbstverwaltung:
    - Fachbereichsrat
    - Institutsrat
    - Kommissionen (Ausbildungskommission, ...)
- Studentische Selbstverwaltung:
    - $\text{Studierendenparlament (StuPa)}^{\text{soon}}$

# Was bringt das

- bindende Prüfungstermine
    - gab es 2 Jahre lang
    - wurde erst entschärft
    - dann quasi abgeschafft
- Mitbestimmen welche Profs lehren

# Sommerfahrt

\begin{center}
\begin{tabular}{c c}
        \includegraphics[width=140px]{Sommerfahrt_See.jpg} &  \includegraphics[width=140px]{Sommerfahrt_podium.jpg} \\
         \includegraphics[width=140px]{Sommerfahrt_Karaoke.jpg} & \includegraphics[width=140px]{Sommerfahrt_steamdeck.jpg}
\end{tabular}
\end{center}

# Karaoke

![Nils](Sommerfahrt_karoke.jpg){ width=300px }

# Lebensqualität am Fachbereich verbessern

![Sofaraum vor Renovierung](Sofaraum_vorher.jpg){ width=350px }

# Lebensqualität am Fachbereich verbessern

![Sofaraum direkt nach Renovierung](Sofaraum_nachher.jpg){ width=350px }

# Organisation

- Antirassistisches, antisexistisches Selbstverständnis
- Keine feste Gruppe von Mitgliedern
- Keine Einstiegshürde
- Alle können vorbeikommen und sich einbringen

# FSI Raum

![Raum K08 im Keller der Takustraße 9](Raum.jpg){ width=300px }

# AStA

![Wir holen AStA-Beutel](Erstibeutel.jpg){ height=250px }

# Plenum

- Treffen alle zwei Wochen, Montags um 18 Uhr im FSI Raum

- Nächstes Plenum **heute um 18:00**, kommt gerne vorbei!

# Kontakt
\begin{tabular}{c c c}
    \includegraphics[width=30mm]{matrix.png} &
    \includegraphics[width=30mm]{telegram.png} &
    \includegraphics[width=30mm]{fsi.png} \\
    Matrix & Telegram & Webseite
\end{tabular}
